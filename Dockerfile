FROM python:3.9

ENV DEBIAN_FRONTEND noninteractive

COPY . /app

RUN pip install uwsgi
RUN pip install --no-cache-dir -r /app/requirements.txt

WORKDIR /app
CMD uwsgi /app/uwsgi-docker.ini
