# Machine Registration Service (MaRS)

MaRS is meant to hold the CI testing infrastructure's topology, and automate
most of its discovery.

## How to deploy a development instance for development purposes

    $ cd <directory of this README file>
    $ docker buildx build -t registry.freedesktop.org/mupuf/mars . --load
    # cp docker.env.sample mars.env
    $ $EDITOR mars.env
    $ docker run --rm --entrypoint bash --env-file mars.env -v $(pwd):/app  -it -v /mnt:/mnt -p 8086:80/tcp --name MaRS registry.freedesktop.org/mupuf/mars

At this point we're in the live container and can develop as usual for
a Django application,

    $ ./manage.py makemigrations
    $ ./manage.py migrate
    $ ./manage.py createsuperuser
    or
    $ DJANGO_SUPERUSER_PASSWORD="password" ./manage.py createsuperuser --username admin --noinput --email=no.email@isp.tld
    $ ./manage.py runserver 0.0.0.0:80

Other handy development notes,

    # Remove all the data from the tables, but not the tables themselves (no need to remigrate)
    ./manage.py flush && env DJANGO_SUPERUSER_PASSWORD="password" ./manage.py createsuperuser --username admin --noinput --email=no.email@isp.tld

    # Create some test data to explore the API for local testing purposes
    ./manage.py loaddata test-data.json

Now, visit the URL `http://$MARS_HOST:8086/admin/`, and you should be
good to go!

### Running MaRS in production using docker

Simply drop the entrypoint override in the development case, and you
have a production instance,

    $ docker run --rm -it --env-file mars.env -v /mnt:/mnt -p 8086:80/tcp --name MaRS registry.freedesktop.org/mupuf/mars

TODO: Add an easy way to set the credentials!

Finally, let's access the admin, using your credentials:

    $ xdg-open http://$MARS_HOST:8086/admin

If all went well, congratulations!
