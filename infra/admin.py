from django.contrib import admin
from .models import PDU, Machine


admin.site.register(PDU)

admin.site.register(Machine)
