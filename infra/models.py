from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.functional import cached_property

from .pdu import PDUApi


class PDU(models.Model):
    name = models.CharField(unique=True, help_text="Name of the PDU", max_length=80,
                            validators=[
                                        RegexValidator(
                                            regex='^[^/]+$', code='invalid_pdu_name',
                                            message='The name of the PDU cannot contain a /',
                                        )])
    url = models.URLField(help_text="URL to the PDU gateway REST service")

    pdu_model = models.CharField(max_length=80, help_text="Model of the PDU you are using")
    config = models.JSONField(blank=True, default=dict,
                              help_text="Opaque configuration needed to instanciate the PDU")

    @cached_property
    def api(self):
        return PDUApi(self)

    def clean(self):
        super().clean()

        if self.pdu_model not in self.api.supported_pdus:
            raise ValidationError(f"The PDU model is unsupported. Needs to be one of {self.api.supported_pdus}")

        try:
            self.api.register()
        except Exception:
            raise ValidationError("Failed to register the PDU on the REST service")

    def __str__(self):
        return f"<PDU: {self.name}>"


class Machine(models.Model):
    # Naming and tagging
    base_name = models.CharField(max_length=50,
                                 help_text="Non-unique name that will form the basis for the full name")
    tags = models.JSONField(blank=True, null=True, default=list,
                            help_text="List of tags that the machine has")

    # Network
    mac_address = \
        models.CharField(max_length=17, unique=True, blank=False, null=False,
                         validators=[
                             RegexValidator(
                                regex='^([0-9A-Fa-f]{2}:){5}([0-9A-Fa-f]{2})$', code='invalid_format',
                                message='Format: 60:6d:3c:51:c4:88',
                                )])
    ip_address = models.GenericIPAddressField(blank=False, null=False)

    # PDU
    pdu = models.ForeignKey(PDU, null=True, blank=True, on_delete=models.SET_NULL,
                            help_text="PDU to which this machine is connected to")
    pdu_port_id = models.CharField(max_length=80, blank=True, null=True,
                                   help_text="ID of the port to which this machine is connected to")

    # Flags
    ready_for_service = models.BooleanField(blank=False, null=False, default=False,
                                            help_text="Is the machine fit for service?")

    # Some statistics
    first_seen = models.DateTimeField(auto_now_add=True,
                                      help_text='When this MAC address was first seen')
    last_updated = models.DateTimeField(auto_now=True,
                                        help_text='When this machine was last modified')

    class Meta:
        ordering = ['-id']

    @cached_property
    def full_name(self):
        if self.pk is None:
            raise ValueError("The Machine needs to be saved before a name can be generated")

        idx = Machine.objects.filter(base_name=self.base_name, id__lte=self.id).count()
        # TODO: add the farm's name
        return f"{self.base_name}-{idx}"

    def clean(self):
        super().clean()

        try:
            ports = self.pdu.api.ports
        except Exception:
            raise ValidationError("Could not talk to the PDU gateway service")

        if str(self.pdu_port_id) not in [str(p) for p in ports.keys()]:
            msg = f"The port '{self.pdu_port_id}' does not exist on {self.pdu}: use any of {list(ports.keys())}"
            raise ValidationError(msg)

    def __str__(self):
        return f"<Machine: {self.full_name}>"
