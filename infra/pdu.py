from django.utils.functional import cached_property
from enum import Enum
import requests


class PDUState(Enum):
    OFF = 'OFF'
    ON = 'ON'
    REBOOT = 'REBOOT'


class PDUPort:
    def __init__(self, pdu, port_id, label):
        self.pdu = pdu
        self.port_id = port_id
        self.label = label

    @property
    def url(self):
        return f"{self.pdu.db_model.url}/v1/pdus/{self.pdu.db_model.name}/ports/{self.port_id}/state"

    @property
    def state(self):
        r = requests.get(self.url)
        r.raise_for_status()
        return PDUState(r.text)

    def set(self, state):
        r = requests.post(self.url, json={"state": state.value})
        r.raise_for_status()

        if r.text != state.value:
            raise ValueError("The new state is different from the expected state")


class PDUApi:
    def __init__(self, db_model):
        self.db_model = db_model

    @cached_property
    def _status(self):
        r = requests.get(self.db_model.url + "/pdu_gateway")

        if r.status_code != 200:
            raise ValueError("The REST interface does not have the mandatory '/pdu_interface'")

        return r.json()

    @property
    def version(self):
        return int(self._status['version'])

    @property
    def supported_pdus(self):
        return self._status.get('supported_pdus', [])

    def register(self):
        params = {
            "model": self.db_model.pdu_model,
            "pdu_name": self.db_model.name,
            "config": self.db_model.config,
        }

        r = requests.post(self.db_model.url + "/v1/pdus", json=params)
        r.raise_for_status()

    @property
    def registered_pdus(self):
        r = requests.get(self.db_model.url + "/v1/pdus")
        r.raise_for_status()
        return r.json().get('pdus', [])

    @property
    def registered(self):
        return self.db_model.name in self.registered_pdus

    @property
    def ports(self):
        r = requests.get(f"{self.db_model.url}/v1/pdus/{self.db_model.name}/ports")
        r.raise_for_status()

        ports = {}
        for port_id, port in r.json().items():
            ports[port_id] = PDUPort(self, port_id, port.get('label'))

        return ports
