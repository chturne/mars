from rest_framework import serializers
from .models import PDU, Machine


class MachineSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedRelatedField(read_only=True, view_name='machine-detail', lookup_field='mac_address')

    full_name = serializers.ReadOnlyField()

    class Meta:
        model = Machine
        exclude = []
        read_only_fields = ['first_seen', 'last_updated']


class PDUSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PDU
        exclude = []
