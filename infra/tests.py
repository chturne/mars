from django.test import TestCase
from django.core.validators import ValidationError
from unittest.mock import patch, MagicMock, PropertyMock

from .models import PDU, Machine
from .pdu import PDUApi, PDUPort, PDUState


class PDUPortTestCase(TestCase):
    def setUp(self):
        self.pdu = PDU(name='PDU1', url="https://my_pdu.local", pdu_model="dummy",
                       config={"ports": ["label 1", "label 2"]})
        self.port = PDUPort(self.pdu.api, 0, "label")

    def test_url(self):
        self.assertEqual(self.port.url, "https://my_pdu.local/v1/pdus/PDU1/ports/0/state")

    @patch("requests.get", return_value=MagicMock(text="REBOOT"))
    def test_state(self, get_mock):
        self.assertEqual(self.port.state, PDUState.REBOOT)
        get_mock.assert_called_with(self.port.url)

    @patch("requests.post", return_value=MagicMock(text="REBOOT"))
    def test_set(self, post_mock):
        expected_params = {
            "state": PDUState.REBOOT.value,
        }
        self.port.set(PDUState.REBOOT)
        post_mock.assert_called_with(self.port.url, json=expected_params)

        with self.assertRaisesMessage(ValueError, "The new state is different from the expected state"):
            self.port.set(PDUState.ON)


class PDUApiTestCase(TestCase):
    status = {
        "supported_pdus": [
            "dummy",
            "cyberpower"
        ],
        "version": 1
    }
    status_mock = MagicMock(status_code=200, json=MagicMock(return_value=status))

    pdus = ["PDU1", "MyPDU 2"]

    ports = {
        0: {
            "label": "My port 0",
            "state": "ON"
        },
        1: {
            "state": "REBOOT"
        },
    }
    ports_mock = MagicMock(status_code=200, json=MagicMock(return_value=ports))

    def setUp(self):
        self.pdu = PDU(name='PDU1', url="https://my_pdu.local", pdu_model="dummy",
                       config={"ports": ["label 1", "label 2"]})

        self.pdu2 = PDU(name='PDU2', url="https://my_pdu.local", pdu_model="dummy",
                        config={"ports": []})

    @patch("requests.get", return_value=MagicMock(status_code=404))
    def test_entrypoint_missing(self, get_mock):
        err_msg = "The REST interface does not have the mandatory '/pdu_interface'"
        with self.assertRaisesMessage(ValueError, err_msg):
            self.pdu.api.version

    @patch("requests.get", return_value=status_mock)
    def test_version(self, get_mock):
        self.assertEqual(self.pdu.api.version, 1)
        get_mock.assert_called_with("https://my_pdu.local/pdu_gateway")

    @patch("requests.get", return_value=status_mock)
    def test_supported_pdus(self, get_mock):
        self.assertEqual(self.pdu.api.supported_pdus, self.status['supported_pdus'])
        get_mock.assert_called_with("https://my_pdu.local/pdu_gateway")

    @patch("requests.post")
    def test_register(self, post_mock):
        expected_params = {
            "model": self.pdu.pdu_model,
            "pdu_name": self.pdu.name,
            "config": self.pdu.config,
        }

        self.pdu.api.register()
        post_mock.assert_called_with("https://my_pdu.local/v1/pdus",
                                     json=expected_params)

    @patch("requests.get", return_value=MagicMock(json=MagicMock(return_value={"pdus": pdus})))
    def test_registered_pdus(self, get_mock):
        self.assertEqual(self.pdu.api.registered_pdus, self.pdus)
        get_mock.assert_called_with("https://my_pdu.local/v1/pdus")

    @patch("requests.get", return_value=MagicMock(json=MagicMock(return_value={"pdus": pdus})))
    def test_registered(self, get_mock):
        self.assertTrue(self.pdu.api.registered)
        self.assertFalse(self.pdu2.api.registered)

    @patch("requests.get", return_value=ports_mock)
    def test_ports(self, get_mock):
        ports = self.pdu.api.ports

        self.assertEqual(len(ports), 2)
        self.assertEqual(ports[0].port_id, 0)
        self.assertEqual(ports[0].label, "My port 0")

        self.assertEqual(ports[1].port_id, 1)
        self.assertEqual(ports[1].label, None)

        get_mock.assert_called_with(f"https://my_pdu.local/v1/pdus/{self.pdu.name}/ports")


class PDUTestCase(TestCase):
    def test_invalid_name(self):
        p = PDU(name='hello/world', url="https://domain.name")
        with self.assertRaises(ValidationError):
            p.clean_fields()

    def test_api(self):
        pdu = PDU(name='PDU1', url="https://domain.name")

        self.assertEqual(pdu.api.db_model, pdu)
        self.assertTrue(isinstance(pdu.api, PDUApi))

    def test_clean__unsupported_pdu_model(self):
        pdu = PDU(pdu_model='PDU1')
        pdu.api = MagicMock(supported_pdus=['pdu1', 'pdu2'])

        msg = "The PDU model is unsupported. Needs to be one of ['pdu1', 'pdu2']"
        with self.assertRaisesMessage(ValidationError, msg):
            pdu.clean()

        pdu = PDU(pdu_model='pdu1')
        pdu.api = MagicMock(supported_pdus=['pdu1', 'pdu2'],
                            register=MagicMock(side_effect=Exception()))
        with self.assertRaisesMessage(ValidationError, "Failed to register the PDU on the REST service"):
            pdu.clean()

    def test_str(self):
        self.assertEqual(str(PDU(name="MyPDU")), "<PDU: MyPDU>")


class MachineTests(TestCase):
    def setUp(self):
        self.pdu = PDU.objects.create(name="MyPDU", pdu_model='PDU1',
                                      url='http://pdu.gateway')

        ports = {
            0: MagicMock(pdu=None, port_id=0),
            42: MagicMock(pdu=None, port_id=42),
        }
        self.pdu.api = MagicMock(ports=ports)

        self.machine = Machine.objects.create(base_name="mupuf-gfx8",
                                              mac_address="de:ad:be:ef:ca:fe",
                                              ip_address="192.168.0.1",
                                              pdu=self.pdu, pdu_port_id="42")

    def test_full_name__on_unsaved_object(self):
        with self.assertRaises(ValueError):
            Machine().full_name

    def test_full_name(self):
        # Check the name of the fixture machine
        self.assertEqual(self.machine.full_name, "mupuf-gfx8-1")

        # Make sure that the index is not affected by other basenames, and
        # increases nicely
        for i in range(1, 9):
            m = Machine.objects.create(base_name="mupuf-gfx9",
                                       mac_address=f"de:ad:be:ef:ca:0{i}",
                                       ip_address="192.168.0.1")
            self.assertEqual(m.full_name, f"mupuf-gfx9-{i}")

    def test_clean__port_exists(self):
        self.machine.clean()

    def test_clean__port_does_not_exist(self):
        self.machine.pdu_port_id = 2
        msg = "The port '2' does not exist on <PDU: MyPDU>: use any of [0, 42]"
        with self.assertRaisesMessage(ValidationError, msg):
            self.machine.clean()

    def test_clean__cant_list_ports(self):
        type(self.pdu.api).ports = PropertyMock(side_effect=ValueError)

        with self.assertRaisesMessage(ValidationError, "Could not talk to the PDU gateway service"):
            self.machine.clean()

    def test_str(self):
        self.assertEqual(str(self.machine), f"<Machine: {self.machine.full_name}>")
