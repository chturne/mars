# from django.shortcuts import render
from rest_framework import viewsets
from .models import PDU, Machine
from .serializers import PDUSerializer, MachineSerializer


class PDUViewSet(viewsets.ModelViewSet):
    queryset = PDU.objects.all()
    serializer_class = PDUSerializer


class MachineViewSet(viewsets.ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer
    lookup_field = 'mac_address'
